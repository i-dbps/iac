# Docker Compose for Development

## Getting started

### Clone the repos

If you have not already done so, you will need to clone, or create a local copy, of the following repos;

```
mkdir iDBPS
cd iDBPS
git clone git@bitbucket.org:i-dbps/iac.git
git clone git@bitbucket.org:i-dbps/web-app.git
git clone git@bitbucket.org:i-dbps/web-srv.git
```


Please note that deploying from `HEAD` (or the latest commit) is **not** stable, and that if you want to do this, you should proceed at your own risk.

For more on how to clone the repo, view [git clone help](https://git-scm.com/docs/git-clone).

Once you have a local copy, run `cd iac` from the root of the project tree.


## Starting the docker containers

Here are the main targets:

- `docker-compose up` - make docker container setup for development (incl. docker-compose.override.yml)
- `docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -` - make docker container setup for production
- `docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.datadash.yml up -` - make docker container setup for production

## Starting the Development Environment
Follow the README in the web-app and web-srv folders for information about starting these services.

### Prerequisites

- [Docker](https://docs.docker.com/engine/installation/) on the host where AWX will be deployed. After installing Docker, the Docker service must be started (depending on your OS, you may have to add the local user that uses Docker to the `docker` group, refer to the documentation for details)
- [docker-compose](https://pypi.org/project/docker-compose/) Python module.
  - This also installs the `docker` Python module, which is incompatible with [`docker-py`](https://pypi.org/project/docker-py/). If you have previously installed `docker-py`, please uninstall it.
- [Docker Compose](https://docs.docker.com/compose/install/).
